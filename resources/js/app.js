require('./bootstrap');

import { createApp } from 'vue'

import router from './router'

import HelloWorld from './components/Welcome'

createApp({
    components: {
        HelloWorld
    }
}).use(router).mount('#app')
