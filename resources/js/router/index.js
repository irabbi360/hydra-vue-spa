import { createRouter, createWebHistory } from 'vue-router';

import AboutPage from '../components/pages/About'

const routes = [
    {
        path: '/about',
        name: 'page.about',
        component: AboutPage
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})